types="A AAAA AFSDB APL CAA CDNSKEY CDS CERT CNAME CSYNC DHCID DLV DNAME DNSKEY DS HIP IPSECKEY KEY KX LOC MX NAPTR NS NSEC NSEC3 NSEC3PARAM OPENPGPKEY PTR RRSIG RP SIG SMIMEA SOA SRV SSHFP TA TKEY TLSA TSIG TXT URI ANY AXFR IXFR OPT MD MF MAILA MB MG MR MINFO MAILB WKS NB NBSTAT NULL A6 NXT KEY SIG HINFO RP X25 ISDN RT NSAP NSAP-PTR PX EID NIMLOC ATMA APL SINK GPOS UINFO UID GID UNSPEC SPF NINFO RKEY TALINK NID L32 L64 LP EUI48 EUI64 DOA"
domains="google.com atlas.cz nix.cz cvut.cz aliexpress.com"

echo -e "byte\tqtype\tdomain"
echo -e "----------------------"

for domain in $domains; do for typ in $types; do echo -e $(host -W 1 -v -t $typ $domain 2>/dev/null | tail -n 1 | cut -d" " -f2)"\t"${typ}"\t"${domain}; done; done | sort -r -n
                                                      