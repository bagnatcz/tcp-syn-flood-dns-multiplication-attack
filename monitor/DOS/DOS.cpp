#include "Header.h"

using namespace std;
#define MY_IP "10.0.0.2"

int main()
{
	pcap_if_t* alldevs;
	pcap_if_t* d;
	int inum;
	int i = 0;
	pcap_t* adhandle;
	char errbuf[PCAP_ERRBUF_SIZE];

	/* Retrieve the device list */
	if (pcap_findalldevs(&alldevs, errbuf) == -1)
	{
		fprintf(stderr, "Error in pcap_findalldevs: %s\n", errbuf);
		exit(1);
	}

	/* Print the list */
	for (d = alldevs; d; d = d->next)
	{
		printf("%d. %s", ++i, d->name);
		if (d->description)
			printf(" (%s)\n", d->description);
		else
			printf(" (No description available)\n");
	}

	if (i == 0)
	{
		printf("\nNo interfaces found! Make sure WinPcap is installed.\n");
		return -1;
	}

	printf("Enter the interface number (1-%d):", i);
	scanf("%d", &inum);

	if (inum < 1 || inum > i)
	{
		printf("\nInterface number out of range.\n");
		/* Free the device list */
		pcap_freealldevs(alldevs);
		return -1;
	}

	/* Jump to the selected adapter */
	for (d = alldevs, i = 0; i < inum - 1; d = d->next, i++);

	/* Open the device */
	/* Open the adapter */
	if ((adhandle = pcap_open_live(d->name,	// name of the device
		65536,			// portion of the packet to capture. 
					   // 65536 grants that the whole packet will be captured on all the MACs.
		1,				// promiscuous mode (nonzero means promiscuous)
		1000,			// read timeout
		errbuf			// error buffer
	)) == NULL)
	{
		fprintf(stderr, "\nUnable to open the adapter. %s is not supported by WinPcap\n", d->name);
		/* Free the device list */
		pcap_freealldevs(alldevs);
		return -1;
	}

	struct bpf_program fcode;
	string filterSYN = "dst host " + (string)MY_IP + " && tcp[tcpflags] & (tcp-syn) != 0";
	string filterDNS = "dst host " + (string)MY_IP + " && udp port 53";
	string filter = "(" + filterSYN + ") || (" + filterDNS + ")";
	if (pcap_compile(adhandle, &fcode, filter.c_str(), 1, 0xffffff) < 0)
	{
		fprintf(stderr, "\nError compiling filter: wrong syntax.\n");

		pcap_close(adhandle);
		return -3;
	}

	//set the filter
	if (pcap_setfilter(adhandle, &fcode) < 0)
	{
		fprintf(stderr, "\nError setting the filter\n");

		pcap_close(adhandle);
		return -4;
	}


	printf("\nlistening on %s...\n", d->description);

	/* At this point, we don't need any more the device list. Free it */
	pcap_freealldevs(alldevs);

	/* start the capture */
	pcap_loop(adhandle, 0, packet_handler, NULL);

	pcap_close(adhandle);
	return 0;
}

DWORD pocet_TCP_ACK = 0, pocet_DNS_req = 0;

/* Callback function invoked by libpcap for every incoming packet */
void packet_handler(u_char* param, const struct pcap_pkthdr* header, const u_char* pkt_data)
{
	struct tm* ltime;
	char timestr[16];
	time_t local_tv_sec;

	/* retireve the position of the ip header */
	ip_header* ih;
	ih = (ip_header*)(pkt_data +
		14); //length of ethernet header

	/* convert the timestamp to readable format */
	local_tv_sec = header->ts.tv_sec;
	ltime = localtime(&local_tv_sec);
	strftime(timestr, sizeof timestr, "%H:%M:%S", ltime);


	if (ih->proto == 6) {
		pocet_TCP_ACK++;
		printf("TCP-ACK (%d) : ", pocet_TCP_ACK);
		
	}
	else if(ih->proto == 17) {
		pocet_DNS_req++;
		printf("DNS-REQ: (%d) ", pocet_DNS_req);
	}

	printf("time: %s,%.6d;\tlen:%d;\tIP adresy:", timestr, header->ts.tv_usec, header->len);
	// print ip addresses and udp ports 
	printf("%d.%d.%d.%d -> %d.%d.%d.%d\n",
		ih->saddr.byte1,
		ih->saddr.byte2,
		ih->saddr.byte3,
		ih->saddr.byte4,
		ih->daddr.byte1,
		ih->daddr.byte2,
		ih->daddr.byte3,
		ih->daddr.byte4);



}